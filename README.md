# TP Docker CI CD Gitlab

## Processus de déploiement

Le projet démarre via l'image "app-container" depuis le dockerfile.

Il expose le port `8089` utilisé depuis le serveur php.

CI/CD:
- la CI permet de construire l'image, lancer les test, et de lancer le container
