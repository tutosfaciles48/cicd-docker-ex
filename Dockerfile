FROM php:8.3 AS base

COPY index.php /var/www/html

EXPOSE 8089

CMD ["php", "-S", "0.0.0.0:8089", "-t", "/var/www/html/"]

FROM base AS test
# Install any needed dependencies
RUN apt-get update && apt-get install -y \
    git \
    wget \
    unzip \
    && rm -rf /var/lib/apt/lists/*

# Install composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Set up the working directory
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install project dependencies using Composer
RUN composer install

# Define the command to run PHPUnit tests
CMD ["vendor/bin/phpunit"]
